/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract_Factory_Pattern;

/**
 *
 * @author bk
 */
class Blue implements Color {

    public Blue() {
    }

    @Override
    public void fill() {
        System.out.println("fill Blue");
    }
    
}
