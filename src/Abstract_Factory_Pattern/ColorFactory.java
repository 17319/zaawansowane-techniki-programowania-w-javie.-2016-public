/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract_Factory_Pattern;

/**
 *
 * @author bk
 */
public class ColorFactory extends AbstractFactory {
	
   @Override
   public Shape getShape(String shapeType){
      return null;
   }
   
   @Override
   Color getColor(String color) {
   
      if(color == null){
         return null;
      }		
      
      if(color.equalsIgnoreCase("RED")){
         return (Color) new Red();
         
      }else if(color.equalsIgnoreCase("GREEN")){
         return (Color) new Green();
         
      }else if(color.equalsIgnoreCase("BLUE")){
         return (Color) new Blue();
      }
      
      return null;
   }
}
