/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract_Factory_Pattern;

/**
 *
 * @author bk
 */
class Green implements Color {

    public Green() {
    }

    @Override
    public void fill() {
        System.out.println("fill Green");
    }
    
}
