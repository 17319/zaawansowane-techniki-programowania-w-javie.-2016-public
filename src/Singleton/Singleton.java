package Singleton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bk
 */
public class Singleton {

  private static Singleton singleton = new Singleton();

  /** A private Constructor prevents any other class from instantiating. */
  private Singleton() {
  }

  /** Static 'instance' method */
  public static Singleton getInstance() {
    return singleton;
  }

  // other methods protected by singleton-ness would be here...

  /** A simple demo method */
  public String demoMethod() {
    return "demo";
  }
}