/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author bk
 */
class Parent
{
    String name;

    public Parent(String n) 
    {
        name = n;
    }
    
}
public class Child extends Parent {
    String name;

    public Child(String n1, String n2) 
    {
        
        super(n1);       //passing argument to parent class constructor
        this.name = n2;
    }
    public void details()
    {
        System.out.println(super.name+" and "+name);
    }
     public static void main(String[] args)
    {
        Child cobj = new Child("Parent","Child");
        cobj.details();
    }
}
