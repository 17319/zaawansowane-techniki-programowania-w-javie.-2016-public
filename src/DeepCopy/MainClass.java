/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DeepCopy;

/**
 *
 * @author bk
 */
public class MainClass {
  public static void main(String[] args) {
    Employee emp1 = new Employee("M", "A");
    emp1.setSalary(40000.0);
    emp1.address = new Address("First Street", "San F", "CA", "93702");
    Employee emp2 = (Employee) emp1.clone();
    printEmployee(emp1);
    printEmployee(emp2);

    emp2.setLastName("Smith");
    emp2.address = new Address("Street", "B", "CA", "93722");

    printEmployee(emp1);
    printEmployee(emp2);

  }

  private static void printEmployee(Employee e) {
    System.out.println(e.getFirstName() + " " + e.getLastName());
    System.out.println(e.address.getAddress());
    System.out.println("Salary: " + e.getSalary());
  }
}

class Employee implements Cloneable {
  private String lastName;

  private String firstName;

  private Double salary;

  public Address address;

  public Employee(String lastName, String firstName) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.address = new Address();
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Double getSalary() {
    return this.salary;
  }

  public void setSalary(Double salary) {
    this.salary = salary;

  }

  public Object clone() {
    Employee emp;
    try {
      emp = (Employee) super.clone();
      emp.address = (Address) address.clone();
    } catch (CloneNotSupportedException e) {
      return null; // will never happen
    }
    return emp;
  }

  public String toString() {
    return this.getClass().getName() + "[" + this.firstName + " " + this.lastName + ", "
        + this.salary + "]";
  }
}

class Address implements Cloneable {
  private String street;

  private String city;

  private String state;

  private String zipCode;

  public Address() {
    this.street = "";
    this.city = "";
    this.state = "";
    this.zipCode = "";
  }

  public Address(String street, String city, String state, String zipCode) {
    this.street = street;
    this.city = city;
    this.state = state;
    this.zipCode = zipCode;
  }

  public Object clone(){
    try {
      return super.clone();
    } catch (CloneNotSupportedException e) {
      return null; // will never happen
    }
  }

  public String getAddress() {
    return this.street + "\n" + this.city + ", " + this.state + " " + this.zipCode;
  }
}
