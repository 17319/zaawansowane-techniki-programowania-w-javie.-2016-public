/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SOLID_InterfaceSegregationPrinciple;

/**
 *
 * @author bk
 */
// interface segregation principle - good example
interface IWorker extends IFeedable, IWorkable {
}

interface IWorkable {
	public void work();
}

interface IFeedable{
	public void eat();
}

class Worker implements IWorkable, IFeedable{
        @Override
	public void work() {
		// ....working
	}

        @Override
	public void eat() {
		//.... eating in launch break
	}
}

class Robot implements IWorkable{
        @Override
	public void work() {
		// ....working
	}
}

class SuperWorker implements IWorkable, IFeedable{
        @Override
	public void work() {
		//.... working much more
	}

        @Override
	public void eat() {
		//.... eating in launch break
	}
}

class Manager {
	IWorkable worker;

	public void setWorker(IWorkable w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
}
