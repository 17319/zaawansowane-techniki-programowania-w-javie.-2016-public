/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SOLID_InterfaceSegregationPrinciple;

/**
 *
 * @author bk
 */
// interface segregation principle - bad example
interface IWorkerISP {
	public void work();
	public void eat();
}

class WorkerISP implements IWorkerISP{
        @Override
	public void work() {
		// ....working
	}
        @Override
	public void eat() {
		// ...... eating in launch break
	}
}

class SuperWorkerISP implements IWorkerISP{
        @Override
	public void work() {
		//.... working much more
	}

        @Override
	public void eat() {
		//.... eating in launch break
	}
}

class ManagerISP {
	IWorkerISP worker;

	public void setWorker(IWorkerISP w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
}
