/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SOLID_SingleResponsibilityPrinciple;

import sun.applet.Main;

/**
 *
 * @author bk
 */
// single responsibility principle - bad example
interface IEmail {

    public void setSender(String sender);

    public void setReceiver(String receiver);

    public void setContent(String content);
}

class Email implements IEmail {

    public void setSender(String sender) {// set sender; 

    }

    @Override
    public void setReceiver(String receiver) {// set receiver; 

    }

    @Override
    public void setContent(String content) {// set content; 
    }

}
