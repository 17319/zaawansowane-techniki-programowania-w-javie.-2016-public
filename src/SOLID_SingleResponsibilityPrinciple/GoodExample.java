/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SOLID_SingleResponsibilityPrinciple;

/**
 *
 * @author bk
 */
// single responsibility principle - good example
interface IEmail2 {
    public void setSender(String sender);
    public void setReceiver(String receiver);
    public void setContent(IContent content);
}

interface IContent {
    public String getAsString(); // used for serialization
}

class Email2 implements IEmail2 {
    @Override
    public void setSender(String sender) {// set sender; 
    }

    @Override
    public void setReceiver(String receiver) {// set receiver;
    }

    @Override
    public void setContent(IContent content) {// set content; 
    }
}
