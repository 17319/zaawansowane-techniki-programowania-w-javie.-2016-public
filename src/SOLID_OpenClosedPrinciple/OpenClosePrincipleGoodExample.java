/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SOLID_OpenClosedPrinciple;

/**
 *
 * @author bk
 */
// Open-Close Principle - Good example
 class GraphicEditor2 {
 	public void drawShape(Shape2 s) {
 		s.draw();
 	}
 }
 
 abstract class Shape2 {
 	abstract void draw();
 }
 
 class Rectangle extends Shape2  {
         @Override
 	public void draw() {
 		// draw the rectangle
 	}
 } 