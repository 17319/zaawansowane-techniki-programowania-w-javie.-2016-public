/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SOLID_OpenClosedPrinciple;

/**
 *
 * @author bk
 */
// Open-Close Principle - Bad example
class GraphicEditor {

    public void drawShape(Shape s) {
        if (s.m_type == 1) {
            drawRectangle((OpenClosePrincipleBadExample) s);
        } else if (s.m_type == 2) {
            drawCircle((Circle) s);
        }
    }

    public void drawCircle(Circle r) {
        System.out.println("draw Circle r  = " + r);
    }

    public void drawRectangle(OpenClosePrincipleBadExample r) {
        System.out.println("draw Rectagle r  = " + r);
    }
}

class Shape {

    int m_type;
}

class OpenClosePrincipleBadExample extends Shape {

    OpenClosePrincipleBadExample() {
        super.m_type = 1;
    }
}

class Circle extends Shape {

    Circle() {
        super.m_type = 2;
    }
}
