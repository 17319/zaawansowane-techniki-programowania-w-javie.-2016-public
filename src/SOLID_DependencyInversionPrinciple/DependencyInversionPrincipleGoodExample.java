/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SOLID_DependencyInversionPrinciple;

/**
 *
 * @author bk
 */
// Dependency Inversion Principle - Good example
interface IWorker {
	public void work();
}

class WorkerDIP implements IWorker{
        @Override
	public void work() {
		// ....working
	}
}

class SuperWorkerDIP  implements IWorker{
        @Override
	public void work() {
		//.... working much more
	}
}

class ManagerDIP {
	IWorker worker;

	public void setWorker(IWorker w) {
		worker = w;
	}

	public void manage() {
		worker.work();
	}
}

