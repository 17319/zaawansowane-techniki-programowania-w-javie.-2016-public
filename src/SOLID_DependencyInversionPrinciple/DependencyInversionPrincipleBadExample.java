/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SOLID_DependencyInversionPrinciple;

/**
 *
 * @author bk
 */
// Dependency Inversion Principle - Bad example

class Worker {

	public void work() {

		// ....working

	}

}



class Manager {

	Worker worker;



	public void setWorker(Worker w) {
		worker = w;
	}

	public void manage() {
		worker.work();
	}
}

class SuperWorker {
	public void work() {
		//.... working much more
	}
}
