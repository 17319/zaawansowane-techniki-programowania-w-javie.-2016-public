
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import pakietOsoba.Rodzaj;
import pakietOsoba.Towar;
import pakietOsoba.Osoba;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bk
 */
public class OknoEdycja extends javax.swing.JFrame {

    int identyfikatorOsoby;
    int identyfikatorRodzaju;
    int identyfikatorTowaru;
    Map hashMapOsoby = new HashMap();
    Map hashMapRzodzajow = new HashMap();
    Map hashMapTowarow = new HashMap();
    Map hashMap = new HashMap();

    public OknoEdycja(String string, int identyfikatorTowaru) {
        initComponents();
        labelTytul.setText(string);
        this.identyfikatorTowaru = identyfikatorTowaru;

        dajTowar();
        dajOsoby();
        dajRodzaje();

        if (identyfikatorTowaru != 0) {
            dajDane(identyfikatorTowaru);
        } else {
            tfTytul.setText("");
        }
    }

    public void dajDane(int identyfikatorTowaru) {
        tfTytul.setText(hashMapTowarow.get(identyfikatorTowaru).toString());

        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();

        List<Towar> listaTowarow = s.createQuery("from Towar").list();
        for (Towar t : listaTowarow) {
            if (t.getIdTowaru() == identyfikatorTowaru) {
                identyfikatorOsoby = t.getIdOs();
                identyfikatorRodzaju = t.getIdRodz();
                break;
            }
        }

        Object osoba = hashMapOsoby.get(identyfikatorOsoby);
        System.out.println(osoba);
        ustawWybranaWartosc(cbDodajacy, (String) osoba);
        Object gatunek = hashMapRzodzajow.get(identyfikatorRodzaju);
        System.out.println(gatunek);
        ustawWybranaWartosc(cbRodzaj, (String) gatunek);
    }

    private OknoEdycja() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfTytul = new javax.swing.JTextField();
        buttonZapiszDoBazy = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        buttonDodajDodajacego = new javax.swing.JButton();
        buttonDodajRodzaj = new javax.swing.JButton();
        cbDodajacy = new javax.swing.JComboBox();
        cbRodzaj = new javax.swing.JComboBox();
        labelTytul = new javax.swing.JLabel();
        buttonAnuluj = new javax.swing.JButton();
        jButtonUsunOsobe = new javax.swing.JButton();
        jButtonUsunRodzaj = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        tfTytul.setText("Title");
        tfTytul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfTytulActionPerformed(evt);
            }
        });

        buttonZapiszDoBazy.setText("Zapisz");
        buttonZapiszDoBazy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonZapiszDoBazyActionPerformed(evt);
            }
        });

        jLabel1.setText("Nazwa:");

        jLabel2.setText("Rodzaj:");

        jLabel3.setText("Dodający:");

        buttonDodajDodajacego.setText("Dodaj nowego zleceniodawcę");
        buttonDodajDodajacego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDodajDodajacegoActionPerformed(evt);
            }
        });

        buttonDodajRodzaj.setText("Dodaj nowy rodzaj produktu");
        buttonDodajRodzaj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDodajRodzajActionPerformed(evt);
            }
        });

        cbDodajacy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbDodajacy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbDodajacyActionPerformed(evt);
            }
        });

        cbRodzaj.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        labelTytul.setText("Dodaj produkt do listy");

        buttonAnuluj.setText("Anuluj");
        buttonAnuluj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAnulujActionPerformed(evt);
            }
        });

        jButtonUsunOsobe.setText("Usuń dodajacego");
        jButtonUsunOsobe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUsunOsobeActionPerformed(evt);
            }
        });

        jButtonUsunRodzaj.setText("Usuń rodzaj");
        jButtonUsunRodzaj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUsunRodzajActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(labelTytul)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(tfTytul, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(buttonDodajRodzaj, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonDodajDodajacego, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbDodajacy, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbRodzaj, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(buttonZapiszDoBazy, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(buttonAnuluj, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonUsunOsobe, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonUsunRodzaj, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelTytul)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfTytul, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbDodajacy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(buttonDodajDodajacego))
                        .addGap(12, 12, 12)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbRodzaj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(buttonDodajRodzaj)
                            .addComponent(jButtonUsunRodzaj)))
                    .addComponent(jButtonUsunOsobe, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(buttonZapiszDoBazy)
                .addGap(40, 40, 40)
                .addComponent(buttonAnuluj)
                .addGap(29, 29, 29))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
private void wstawOsobeDoBazy(String nazwa) {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Osoba o = new Osoba();
        String[] tablicaStringow = nazwa.split("\\s+");
        for (int i = 0; i < tablicaStringow.length; i++) {
            tablicaStringow[i] = tablicaStringow[i].replaceAll("[^\\w]", "");
        }

        String nazwisko;
        String imie = tablicaStringow[0];
        if (tablicaStringow.length > 2) {
            int j;
            for (j = 1; j < tablicaStringow.length - 1; j++) {
                imie = imie + " " + tablicaStringow[j];
            }
            nazwisko = tablicaStringow[j];
        } else {
            nazwisko = tablicaStringow[1];
        }

        o.setImie(imie);
        o.setNazwisko(nazwisko);
        s.save(o);
        s.getTransaction().commit();
        s.close();
    }

    private void dodajRodzaj(String nazwa) {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Rodzaj r = new Rodzaj();
        r.setNazwaRodzaju(nazwa);
        s.save(r);
        s.getTransaction().commit();
        s.close();
    }

    private void buttonDodajDodajacegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDodajDodajacegoActionPerformed
        String nowaOsoba = JOptionPane.showInputDialog(null, "Podaj dane nowego zleceniodawcy: ");

        if (nowaOsoba != null && !nowaOsoba.isEmpty()) {
            wstawOsobeDoBazy(nowaOsoba);
            dajOsoby();
        }


    }//GEN-LAST:event_buttonDodajDodajacegoActionPerformed

    private void buttonDodajRodzajActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDodajRodzajActionPerformed
        String nowyProdukt = JOptionPane.showInputDialog(null, "Podaj nowy rodzaj produktu: ");
        if (nowyProdukt != null && !nowyProdukt.isEmpty()) {
            dodajRodzaj(nowyProdukt);
            dajRodzaje();
        }
    }//GEN-LAST:event_buttonDodajRodzajActionPerformed

    private void buttonZapiszDoBazyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonZapiszDoBazyActionPerformed
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        identyfikatorOsoby = (int) dajKlucz(hashMapOsoby, cbDodajacy.getSelectedItem().toString());
        identyfikatorRodzaju = (int) dajKlucz(hashMapRzodzajow, cbRodzaj.getSelectedItem().toString());

        if (!tfTytul.getText().trim().isEmpty()) {
            if (identyfikatorTowaru == 0) {
                Towar t = new Towar();
                t.setNazwaTowaru(tfTytul.getText());
                t.setIdOs(identyfikatorOsoby);
                t.setIdRodz(identyfikatorRodzaju);
                s.save(t);

            } else {
                Query query = s.createQuery("update Towar set nazwa_towaru = '" + tfTytul.getText() + "', id_os = " + identyfikatorOsoby + ", id_rodz = " + identyfikatorRodzaju + " where id_towaru = " + identyfikatorTowaru);
                int result = query.executeUpdate();
            }
        } else {
            JFrame uwaga = new JFrame("Uwaga");
            JOptionPane.showMessageDialog(uwaga, "Wprowadz nazwę towaru", "Uwaga", JOptionPane.WARNING_MESSAGE);
        }

        s.getTransaction().commit();
        s.close();
        this.dispose();
    }//GEN-LAST:event_buttonZapiszDoBazyActionPerformed

    private void buttonAnulujActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAnulujActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonAnulujActionPerformed

    private void cbDodajacyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbDodajacyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbDodajacyActionPerformed

    private void tfTytulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfTytulActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfTytulActionPerformed

    private void jButtonUsunOsobeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUsunOsobeActionPerformed

        try {
            JFrame uwaga = new JFrame("Uwaga");
            int n = JOptionPane.showConfirmDialog(uwaga, "Czy napewno chcesz usunąć dodającego?", "Uwaga", JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                String nazwa = cbDodajacy.getSelectedItem().toString();
                String[] nzw = nazwa.split(" ");

                System.out.println(nazwa);// getSelectedValue().toString();
                //         int identyfikatorDodającego;
                // identyfikatorDodającego = (int) dajKlucz(hashMap, nazwa);
                Session s;
                s = NewHibernateUtil.getSessionFactory().openSession();
                s.beginTransaction();
                Query query = s.createQuery("DELETE FROM Osoba WHERE Imie='" + nzw[0] + "' and nazwisko ='" + nzw[1]+"'");
                int result = query.executeUpdate();
                s.getTransaction().commit();
                s.flush();
                s.close();
                dajTowar();
                this.dispose();
            }
        } catch (Exception e) {
            System.out.println("Usuwasz osobe która ma przypisane produkty");
            JOptionPane.showMessageDialog(null, "Usuwasz osobe która ma przypisane produkty");
        } finally {
         //   this.dispose();
        }
    }//GEN-LAST:event_jButtonUsunOsobeActionPerformed

    private void jButtonUsunRodzajActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUsunRodzajActionPerformed
        try {
            JFrame uwaga = new JFrame("Uwaga");
            int n = JOptionPane.showConfirmDialog(uwaga, "Czy napewno chcesz usunąć rodzaj?", "Uwaga", JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                String nazwa = cbRodzaj.getSelectedItem().toString();
                

                System.out.println(nazwa);// getSelectedValue().toString();
                Session s;
                s = NewHibernateUtil.getSessionFactory().openSession();
                s.beginTransaction();
                Query query = s.createQuery("DELETE FROM Rodzaj WHERE nazwa_rodzaju='" + nazwa +"'");
                int result = query.executeUpdate();
                s.getTransaction().commit();
                s.flush();
                s.close();
                dajTowar();
                this.dispose();
            }
        } catch (Exception e) {
            System.out.println("Usuwasz rodzaj który ma przypisane produkty");
            JOptionPane.showMessageDialog(null, "Usuwasz rodzaj który ma przypisane produkty");
        } finally {
            //this.dispose();
        }
    }//GEN-LAST:event_jButtonUsunRodzajActionPerformed

    private void dajOsoby() {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();

        List<Osoba> listaOsob = s.createQuery("from Osoba").list();
        Vector listaOsobCB = new Vector();

        for (Osoba o : listaOsob) {
            listaOsobCB.add(o.getImie() + " " + o.getNazwisko());
            hashMapOsoby.put(o.getIdOsoby(), o.getImie() + " " + o.getNazwisko());
        }
        final DefaultComboBoxModel modecCB = new DefaultComboBoxModel(listaOsobCB);
        cbDodajacy.setModel(modecCB);
        s.close();
    }

    private void dajRodzaje() {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();

        List<Rodzaj> listaRodzajow = s.createQuery("from Rodzaj").list();
        Vector listaRodzajowCB = new Vector();

        for (Rodzaj r : listaRodzajow) {
            listaRodzajowCB.add(r.getNazwaRodzaju());
            hashMapRzodzajow.put(r.getIdRodzaju(), r.getNazwaRodzaju());
        }
        final DefaultComboBoxModel combomodel = new DefaultComboBoxModel(listaRodzajowCB);
        cbRodzaj.setModel(combomodel);
        s.close();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OknoEdycja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OknoEdycja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OknoEdycja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OknoEdycja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new OknoEdycja().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAnuluj;
    private javax.swing.JButton buttonDodajDodajacego;
    private javax.swing.JButton buttonDodajRodzaj;
    private javax.swing.JButton buttonZapiszDoBazy;
    private javax.swing.JComboBox cbDodajacy;
    private javax.swing.JComboBox cbRodzaj;
    private javax.swing.JButton jButtonUsunOsobe;
    private javax.swing.JButton jButtonUsunRodzaj;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel labelTytul;
    private javax.swing.JTextField tfTytul;
    // End of variables declaration//GEN-END:variables

    public static Object dajKlucz(Map hashMapa, Object wartosc) {
        for (Object o : hashMapa.keySet()) {
            if (hashMapa.get(o).equals(wartosc)) {
                return o;
            }
        }
        return null;
    }

    private void dajTowar() {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<Towar> listaTowarow = s.createQuery("from Towar").list();

        for (Towar t : listaTowarow) {
            hashMapTowarow.put(t.getIdTowaru(), t.getNazwaTowaru());
        }
        s.close();
    }

    public static void ustawWybranaWartosc(JComboBox comboBox, String string) {
        String item;
        for (int i = 0; i < comboBox.getItemCount(); i++) {
            item = comboBox.getItemAt(i).toString();
            if (item.equals(string)) {
                comboBox.setSelectedIndex(i);
                break;
            }
        }
    }

}
