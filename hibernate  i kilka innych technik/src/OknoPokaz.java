
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import pakietOsoba.Rodzaj;
import pakietOsoba.Towar;
import pakietOsoba.Osoba;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bk
 */
public class OknoPokaz extends javax.swing.JFrame {

    int identyfikatorOsoby;
    int identyfikatorRodzaju;
    int identyfikatorTowaru;
    Map hashMapOsoby = new HashMap();
    Map hashMapRzodzajow = new HashMap();
    Map hashMapTowarow = new HashMap();

    public OknoPokaz(String string, int identyfikatorTowaru) {
        initComponents();
        labelTytul.setText(string);
        this.identyfikatorTowaru = identyfikatorTowaru;

        dajTowar();
        dajOsoby();
        dajRodzaje();

        if (identyfikatorTowaru != 0) {
            dajDane(identyfikatorTowaru);
        } else {
            tfTytul.setText("");
        }
    }

    public void dajDane(int identyfikatorTowaru) {
        tfTytul.setText(hashMapTowarow.get(identyfikatorTowaru).toString());

        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();

        List<Towar> listaTowarow = s.createQuery("from Towar").list();
        for (Towar t : listaTowarow) {
            if (t.getIdTowaru() == identyfikatorTowaru) {
                identyfikatorOsoby = t.getIdOs();
                identyfikatorRodzaju = t.getIdRodz();
                break;
            }
        }

        Object osoba = hashMapOsoby.get(identyfikatorOsoby);
        System.out.println(osoba);
        ustawWybranaWartosc(cbDodajacy, (String) osoba);
        Object gatunek = hashMapRzodzajow.get(identyfikatorRodzaju);
        System.out.println(gatunek);
        ustawWybranaWartosc(cbRodzaj, (String) gatunek);
    }

    private OknoPokaz() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfTytul = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cbDodajacy = new javax.swing.JComboBox();
        cbRodzaj = new javax.swing.JComboBox();
        labelTytul = new javax.swing.JLabel();
        buttonAnuluj = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        tfTytul.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tfTytul.setForeground(new java.awt.Color(255, 255, 255));
        tfTytul.setText("Title");
        tfTytul.setEnabled(false);
        tfTytul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfTytulActionPerformed(evt);
            }
        });

        jLabel1.setText("Nazwa:");

        jLabel2.setText("Rodzaj:");

        jLabel3.setText("Dodający:");

        cbDodajacy.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbDodajacy.setForeground(new java.awt.Color(255, 255, 255));
        cbDodajacy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbDodajacy.setEnabled(false);
        cbDodajacy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbDodajacyActionPerformed(evt);
            }
        });

        cbRodzaj.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbRodzaj.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbRodzaj.setEnabled(false);

        labelTytul.setText("Szczegóły produktu");

        buttonAnuluj.setText("Zakmnij");
        buttonAnuluj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAnulujActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(labelTytul)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(tfTytul)
                    .addComponent(cbDodajacy, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbRodzaj, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonAnuluj, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelTytul)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfTytul, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbDodajacy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbRodzaj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(buttonAnuluj, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
private void wstawOsobeDoBazy(String nazwa) {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Osoba o = new Osoba();
        String[] tablicaStringow = nazwa.split("\\s+");
        for (int i = 0; i < tablicaStringow.length; i++) {
            tablicaStringow[i] = tablicaStringow[i].replaceAll("[^\\w]", "");
        }

        String nazwisko;
        String imie = tablicaStringow[0];
        if (tablicaStringow.length > 2) {
            int j;
            for (j = 1; j < tablicaStringow.length - 1; j++) {
                imie = imie + " " + tablicaStringow[j];
            }
            nazwisko = tablicaStringow[j];
        } else {
            nazwisko = tablicaStringow[1];
        }

        o.setImie(imie);
        o.setNazwisko(nazwisko);
        s.save(o);
        s.getTransaction().commit();
        s.close();
    }

    private void dodajRodzaj(String nazwa) {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Rodzaj r = new Rodzaj();
        r.setNazwaRodzaju(nazwa);
        s.save(r);
        s.getTransaction().commit();
        s.close();
    }

    private void buttonAnulujActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAnulujActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonAnulujActionPerformed

    private void cbDodajacyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbDodajacyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbDodajacyActionPerformed

    private void tfTytulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfTytulActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfTytulActionPerformed

    private void dajOsoby() {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();

        List<Osoba> listaOsob = s.createQuery("from Osoba").list();
        Vector listaOsobCB = new Vector();

        for (Osoba o : listaOsob) {
            listaOsobCB.add(o.getImie() + " " + o.getNazwisko());
            hashMapOsoby.put(o.getIdOsoby(), o.getImie() + " " + o.getNazwisko());
        }
        final DefaultComboBoxModel modecCB = new DefaultComboBoxModel(listaOsobCB);
        cbDodajacy.setModel(modecCB);
        s.close();
    }

    private void dajRodzaje() {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();

        List<Rodzaj> listaRodzajow = s.createQuery("from Rodzaj").list();
        Vector listaRodzajowCB = new Vector();

        for (Rodzaj r : listaRodzajow) {
            listaRodzajowCB.add(r.getNazwaRodzaju());
            hashMapRzodzajow.put(r.getIdRodzaju(), r.getNazwaRodzaju());
        }
        final DefaultComboBoxModel combomodel = new DefaultComboBoxModel(listaRodzajowCB);
        cbRodzaj.setModel(combomodel);
        s.close();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OknoEdycja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OknoEdycja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OknoEdycja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OknoEdycja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new OknoPokaz().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAnuluj;
    private javax.swing.JComboBox cbDodajacy;
    private javax.swing.JComboBox cbRodzaj;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel labelTytul;
    private javax.swing.JTextField tfTytul;
    // End of variables declaration//GEN-END:variables

    public static Object dajKlucz(Map hashMapa, Object wartosc) {
        for (Object o : hashMapa.keySet()) {
            if (hashMapa.get(o).equals(wartosc)) {
                return o;
            }
        }
        return null;
    }

    private void dajTowar() {
        Session s;
        s = NewHibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        List<Towar> listaTowarow = s.createQuery("from Towar").list();

        for (Towar t : listaTowarow) {
            hashMapTowarow.put(t.getIdTowaru(), t.getNazwaTowaru());
        }
        s.close();
    }

    public static void ustawWybranaWartosc(JComboBox comboBox, String string) {
        String item;
        for (int i = 0; i < comboBox.getItemCount(); i++) {
            item = comboBox.getItemAt(i).toString();
            if (item.equals(string)) {
                comboBox.setSelectedIndex(i);
                break;
            }
        }
    }

}
