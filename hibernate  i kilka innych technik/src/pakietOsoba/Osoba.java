package pakietOsoba;
// Generated 2015-12-07 09:25:07 by Hibernate Tools 3.6.0



/**
 * Osoba generated by hbm2java
 */
public class Osoba  implements java.io.Serializable {


     private int idOsoby;
     private String imie;
     private String nazwisko;

    public Osoba() {
    }

    public Osoba(int idOsoby, String imie, String nazwisko) {
       this.idOsoby = idOsoby;
       this.imie = imie;
       this.nazwisko = nazwisko;
    }
   
    public int getIdOsoby() {
        return this.idOsoby;
    }
    
    public void setIdOsoby(int idOsoby) {
        this.idOsoby = idOsoby;
    }
    public String getImie() {
        return this.imie;
    }
    
    public void setImie(String imie) {
        this.imie = imie;
    }
    public String getNazwisko() {
        return this.nazwisko;
    }
    
    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }




}


